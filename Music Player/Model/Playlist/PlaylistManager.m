//
//  PlaylistManager.m
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "PlaylistManager.h"

@implementation PlaylistManager

+ (instancetype)sharedPlaylist {
    static PlaylistManager *sharedMyPlaylist = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyPlaylist = [[PlaylistManager alloc] init];
    });
    return sharedMyPlaylist;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSError *error;
        Song *fiction = [[Song alloc] initWithTitle:@"Fiction" artist:@"Tim Kubak" identifier:8 fileName:@"Fiction" fileType:@".mp3" error:&error];
        Song *happyStep = [[Song alloc] initWithTitle:@"Happy Step" artist:@"Nordgroove" identifier:9 fileName:@"Happy-Step" fileType:@".mp3" error:&error];
        Song *bodyPacker = [[Song alloc] initWithTitle:@"Body Packer" artist:@"LoveFine" identifier:4 fileName:@"Body-Packer" fileType:@".mp3" error:&error];
        Song *stop = [[Song alloc] initWithTitle:@"Stop" artist:@"Max Sergeev" identifier:12 fileName:@"Stop" fileType:@".mp3" error:&error];
        Song *theRoads = [[Song alloc] initWithTitle:@"The Roads" artist:@"Ilya Truhanov" identifier:12 fileName:@"The-Roads" fileType:@".mp3" error:&error];
        Playlist *playlist = [[Playlist alloc] initWithIdentifier:1 name:@"Playlist 1" songs:[NSMutableArray arrayWithObjects:fiction, happyStep, bodyPacker, stop, theRoads, nil] error:&error];
        if (!_playlists) {
            _playlists = [[NSMutableArray alloc] init];
        }
        [_playlists addObject:playlist];
    }
    return self;
}

@end
