//
//  Playlist.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Song.h"

NS_ASSUME_NONNULL_BEGIN

@interface Playlist : NSObject

/** Properties */
@property (nonatomic) NSInteger identifier;
@property (nonatomic) NSString *name;
@property (nonatomic, nullable) NSMutableArray <Song *> *songs;

/**
 Initialising playlist object

 @param identifier NSinteger
 @param name NSString
 @param songs NSMutableArray <Song>
 @param outError NSError
 @return instancetype
 */
- (instancetype)initWithIdentifier:(NSInteger)identifier name:(NSString *)name songs:(NSMutableArray <Song *> * _Nullable )songs error:(NSError * _Nullable *)outError;

@end

NS_ASSUME_NONNULL_END
