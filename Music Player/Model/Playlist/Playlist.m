//
//  Playlist.m
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "Playlist.h"

@implementation Playlist

- (instancetype)initWithIdentifier:(NSInteger)identifier name:(NSString *)name songs:(NSMutableArray <Song *> * _Nullable )songs error:(NSError * _Nullable *)outError {
    self = [super init];
    if (self) {
        self.identifier = identifier;
        self.name = name;
        self.songs = songs;
    }
    return self;
}

@end
