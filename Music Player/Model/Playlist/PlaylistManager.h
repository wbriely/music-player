//
//  PlaylistManager.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Playlist.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaylistManager : NSObject

/** Properties */
@property (nonatomic) NSMutableArray <Playlist *> *playlists;

/**
 Accessing properties of PlaylistManager

 @return instancetype
 */
+ (instancetype)sharedPlaylist;

@end

NS_ASSUME_NONNULL_END
