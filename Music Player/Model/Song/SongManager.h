//
//  SongManager.h
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import "Song.h"

NS_ASSUME_NONNULL_BEGIN

@interface SongManager : NSObject

/** Properties */
@property (readonly, nonatomic) NSArray <Song *> *songs;

/**
 Accessing properties of SongManager

 @return instancetype
 */
+ (instancetype)sharedManager;

@end

NS_ASSUME_NONNULL_END
