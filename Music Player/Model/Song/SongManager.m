//
//  SongManager.m
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "SongManager.h"

@implementation SongManager

+ (instancetype)sharedManager {
    static SongManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[SongManager alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSError *error;
        Song *afraidOfDestiny = [[Song alloc] initWithTitle:@"Afraid of Destiny" artist:@"Denis Stelmach" identifier:1 fileName:@"Afraid-of-Destiny" fileType:@".mp3" error:&error];
        Song *auCinema = [[Song alloc] initWithTitle:@"Au Cinema" artist:@"Svyat Ilin" identifier:2 fileName:@"Au-Cinema" fileType:@".mp3" error:&error];
        Song *beautyGazer = [[Song alloc] initWithTitle:@"Beauty Gazer" artist:@"Ilya Marfin" identifier:3 fileName:@"Beauty-Gazer" fileType:@".mp3" error:&error];
        Song *bodyPacker = [[Song alloc] initWithTitle:@"Body Packer" artist:@"LoveFine" identifier:4 fileName:@"Body-Packer" fileType:@".mp3" error:&error];
        Song *cetacean = [[Song alloc] initWithTitle:@"Cetacean" artist:@"RZRS" identifier:5 fileName:@"Cetacean" fileType:@".mp3" error:&error];
        Song *deny = [[Song alloc] initWithTitle:@"Deny" artist:@"Denis Stelmach" identifier:6 fileName:@"Deny" fileType:@".mp3" error:&error];
        Song *fame = [[Song alloc] initWithTitle:@"Fame" artist:@"Savvier" identifier:7 fileName:@"Fame" fileType:@".mp3" error:&error];
        Song *fiction = [[Song alloc] initWithTitle:@"Fiction" artist:@"Tim Kubak" identifier:8 fileName:@"Fiction" fileType:@".mp3" error:&error];
        Song *happyStep = [[Song alloc] initWithTitle:@"Happy Step" artist:@"Nordgroove" identifier:9 fileName:@"Happy-Step" fileType:@".mp3" error:&error];
        Song *lostControl = [[Song alloc] initWithTitle:@"Lost Control" artist:@"Spaceinvader" identifier:10 fileName:@"Lost-Control" fileType:@".mp3" error:&error];
        Song *primary = [[Song alloc] initWithTitle:@"Primary" artist:@"Bajun" identifier:11 fileName:@"Primary" fileType:@".mp3" error:&error];
        Song *stop = [[Song alloc] initWithTitle:@"Stop" artist:@"Max Sergeev" identifier:12 fileName:@"Stop" fileType:@".mp3" error:&error];
        Song *theRoads = [[Song alloc] initWithTitle:@"The Roads" artist:@"Ilya Truhanov" identifier:12 fileName:@"The-Roads" fileType:@".mp3" error:&error];
        Song *voiceless = [[Song alloc] initWithTitle:@"Voiceless" artist:@"Stage Engine" identifier:13 fileName:@"Voiceless" fileType:@".mp3" error:&error];

        _songs = @[afraidOfDestiny, auCinema, beautyGazer, bodyPacker, cetacean, deny, fame, fiction, happyStep, lostControl, primary, stop, theRoads, voiceless];
    }
    return self;
}

@end
