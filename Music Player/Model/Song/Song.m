//
//  Song.m
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "Song.h"

@implementation Song

- (instancetype)initWithTitle:(NSString *)title artist:(NSString *)artist identifier:(NSInteger)identifier fileName:(NSString *)fileName fileType:(NSString *)fileType error:(NSError * _Nullable *)outError {
    self = [super init];
    if (self) {
        self.title = title;
        self.artist = artist;
        self.identifier = identifier;
        self.fileName = fileName;
        self.fileType = fileType;
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:outError];
    }
    return self;
}

@end
