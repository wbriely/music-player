//
//  Song.h
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Song : NSObject

/** Properties */
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger identifier;
@property (nonatomic) NSString *fileName;
@property (nonatomic) NSString *artist;
@property (nonatomic) NSString *fileType;
@property (nonatomic, nullable) AVAudioPlayer *player;


/**
 Initialising song object

 @param title NSString
 @param fileName NSString
 @param fileType NSString
 @param outError NSError
 @return instancetype
 */
- (instancetype)initWithTitle:(NSString *)title artist:(NSString *)artist identifier:(NSInteger)identifier fileName:(NSString *)fileName fileType:(NSString *)fileType error:(NSError * _Nullable *)outError;

@end

NS_ASSUME_NONNULL_END
