//
//  Formatting.m
//  Music App Objc
//
//  Created by Will Briely on 31/01/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "Formatting.h"

@implementation Formatting

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

@end
