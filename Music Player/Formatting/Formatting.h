//
//  Formatting.h
//  Music App Objc
//
//  Created by Will Briely on 31/01/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Formatting : NSObject

/**
 Format time string to HH:MM:SS for song duration

 @param interval NSTimeInterval
 @return NSString
 */
+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;

@end

NS_ASSUME_NONNULL_END
