//
//  AddToPlaylistViewController.m
//  Music Player
//
//  Created by Will Briely on 27/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "AddToPlaylistViewController.h"

@interface AddToPlaylistViewController ()

@end

@implementation AddToPlaylistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.addToPlaylistViewModel = [[AddToPlaylistTableViewModel alloc] initWithTableView:self.tableView];
    self.addToPlaylistViewModel.delegate = self;
    [self.addToPlaylistViewModel songToAddToPlaylist:self.songList[self.selectedSongIndex]];
}


#pragma mark - MusicPlayerDelegate
- (void)didPressPlaylist:(Playlist *)playlist {
    self.playlist = playlist;
    [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - IBActions
- (IBAction)didPressCancelButton:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
