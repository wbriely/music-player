//
//  AddToPlaylistViewController.h
//  Music Player
//
//  Created by Will Briely on 27/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AddToPlaylistTableViewModel.h"
#import "MusicPlayerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddToPlaylistViewController : UIViewController <MusicPlayerDelegate>

/** Outlets */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** Properties */
@property (nonatomic) AddToPlaylistTableViewModel *addToPlaylistViewModel;
@property (nonatomic) Playlist *playlist;
@property (nonatomic) NSArray <Song *> *songList;
@property (nonatomic) NSInteger selectedSongIndex;

@end

NS_ASSUME_NONNULL_END
