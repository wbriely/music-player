//
//  PlaybackViewController.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Song.h"
#import "Formatting.h"
#import "AddToPlaylistViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaybackViewController : UIViewController

/** Outlets */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

/** Properties */
@property (nonatomic) NSString *addToPlaylistSegueIdentifier;
@property (nonatomic) NSInteger songIndex;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSArray <Song *> *songList;
@property (nonatomic) Song *song;

@end

NS_ASSUME_NONNULL_END
