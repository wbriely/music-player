//
//  PlaybackViewController.m
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "PlaybackViewController.h"

@interface PlaybackViewController ()

@end

@implementation PlaybackViewController

- (NSString *)addToPlaylistSegueIdentifier {
    return @"AddToPlaylistSegue";
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.song = self.songList[self.songIndex];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayback error:&error];
    [self updateView];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: self.addToPlaylistSegueIdentifier]) {
        AddToPlaylistViewController *controller = segue.destinationViewController;
        if (controller) {
            controller.selectedSongIndex = self.songIndex;
            controller.songList = self.songList;
        }
    }
}


#pragma mark - Updating View
- (void)updateTimer {
    double currentTime = self.song.player.currentTime;
    if (currentTime) {
        NSString *formattedCurrentTime = [Formatting stringFromTimeInterval:currentTime];
        self.currentTimeLabel.text = [NSString stringWithFormat:@"%@", formattedCurrentTime];
        [self.progressView setProgress:currentTime/self.song.player.duration animated:true];
    }
}

- (void)updateView {
    self.titleLabel.text = self.song.title;
    NSString *formattedDuration = [Formatting stringFromTimeInterval:self.song.player.duration];
    self.durationLabel.text = [NSString stringWithFormat:@"%@", formattedDuration];

    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(updateTimer) userInfo:nil repeats:true];
    });
    if ([self.song.player isPlaying]) {
        [self.playButton setImage:[UIImage imageNamed:@"Pause"] forState:UIControlStateNormal];
    } else {
        [self.playButton setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
    }
}


#pragma mark - IBActions
- (IBAction)didPressPreviousButton:(id)sender {
    [self.song.player stop];
    self.song.player.currentTime = 0;
    self.songIndex = self.songIndex - 1;
    if (self.song == [self.songList firstObject]) {
        self.songIndex = self.songList.count-1;
    }
    self.song = self.songList[self.songIndex];
    [self.song.player play];
    [self updateView];
}

- (IBAction)didPressPlayButton:(id)sender {
    if ([self.song.player isPlaying]) {
        [self.song.player stop];
        [self.playButton setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
    } else {
        [self.song.player play];
        [self.playButton setImage:[UIImage imageNamed:@"Pause"] forState:UIControlStateNormal];
    }
}

- (IBAction)didPressSkipButton:(id)sender {
    [self.song.player stop];
    self.song.player.currentTime = 0;
    self.songIndex++;
    if (self.songIndex == self.songList.count) {
        self.songIndex = 0;
    }
    self.song = self.songList[self.songIndex];
    [self.song.player play];
    [self updateView];
}

- (IBAction)didPressShuffleButton:(id)sender {
    NSUInteger randomIndex = arc4random() % self.songList.count;
    [self.song.player stop];
    self.song.player.currentTime = 0;
    self.song = self.songList[randomIndex];
    [self.song.player play];
    [self updateView];
}

- (IBAction)didPressAddToPlaylistButton:(id)sender {
    [self performSegueWithIdentifier:self.addToPlaylistSegueIdentifier sender:self];
}

@end
