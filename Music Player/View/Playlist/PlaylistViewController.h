//
//  PlaylistViewController.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Playlist.h"
#import "PlaylistTableViewModel.h"
#import "MusicPlayerDelegate.h"
#import "PlaybackViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaylistViewController : UIViewController <MusicPlayerDelegate>

/** Outlets */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** Properties */
@property (nonatomic) NSInteger selectedPlaylistIndex;
@property (nonatomic) NSMutableArray <Playlist *> *playlists;
@property (nonatomic) PlaylistTableViewModel *viewModel;
@property (nonatomic) NSArray <Song *> *songList;
@property (nonatomic) NSInteger selectedSongIndex;
@property (nonatomic) NSString *segueIdentifier;

@end

NS_ASSUME_NONNULL_END
