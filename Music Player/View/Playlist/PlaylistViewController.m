//
//  PlaylistViewController.m
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "PlaylistViewController.h"

@interface PlaylistViewController ()

@end

@implementation PlaylistViewController

- (NSString *)segueIdentifier {
    return @"PlaybackFromPlaylistSegue";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[PlaylistTableViewModel alloc] initWithTableView:self.tableView];
    self.viewModel.delegate = self;
    [self.viewModel selectedPlaylist:self.playlists[self.selectedPlaylistIndex]];
    self.title = self.playlists[self.selectedPlaylistIndex].name;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: self.segueIdentifier]) {
        PlaybackViewController *controller = segue.destinationViewController;
        if (controller) {
            controller.songIndex = self.selectedSongIndex;
            controller.songList = self.songList;
        }
    }
}


#pragma mark - MusicPlayerDelegate
- (void)didPassSongList:(NSArray<Song *> *)songList songIndex:(NSInteger)index {
    self.songList = songList;
    self.selectedSongIndex = index;
    [self performSegueWithIdentifier:self.segueIdentifier sender:self];
}

@end
