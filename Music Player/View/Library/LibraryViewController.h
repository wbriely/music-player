//
//  ViewController.h
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LibraryTableViewModel.h"
#import "MusicPlayerDelegate.h"
#import "PlaybackViewController.h"

@interface LibraryViewController : UIViewController <MusicPlayerDelegate>

/** Outlets */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** Properties */
@property (nonatomic) LibraryTableViewModel *viewModel;
@property (nonatomic) NSString *PlaybackSegueIdentifier;
@property (nonatomic) NSString *PlaylistMenuSegueIdentifier;
@property (nonatomic) NSArray <Song *> *songList;
@property (nonatomic) NSInteger selectedSongIndex;

@end

