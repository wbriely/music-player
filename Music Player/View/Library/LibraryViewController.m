//
//  ViewController.m
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "LibraryViewController.h"

@interface LibraryViewController ()

@end

@implementation LibraryViewController

- (NSString *)PlaybackSegueIdentifier {
    return @"PlaybackSegue";
}

- (NSString *)PlaylistMenuSegueIdentifier {
    return @"PlaylistMenuSegue";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[LibraryTableViewModel alloc] initWithTableView:self.tableView];
    self.viewModel.delegate = self;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: self.PlaybackSegueIdentifier]) {
        PlaybackViewController *controller = segue.destinationViewController;
        if (controller) {
            controller.songIndex = self.selectedSongIndex;
            controller.songList = self.songList;
        }
    }
}


#pragma mark - MusicPlayerDelegate
- (void)didPassSongList:(NSArray<Song *> *)songList songIndex:(NSInteger)index {
    self.songList = songList;
    self.selectedSongIndex = index;
    [self performSegueWithIdentifier:self.PlaybackSegueIdentifier sender:self];
}


#pragma mark - IBActions
- (IBAction)didPressPlaylistMenuButton:(id)sender {
    [self performSegueWithIdentifier:self.PlaylistMenuSegueIdentifier sender:self];
}

@end
