//
//  PlaylistMenuViewController.m
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "PlaylistMenuViewController.h"

@interface PlaylistMenuViewController ()

@end

@implementation PlaylistMenuViewController

- (NSString *)PlaylistSegueIdentifier {
    return @"PlaylistSegue";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[PlaylistMenuTableViewModel alloc] initWithTableView:self.tableView];
    self.viewModel.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: self.PlaylistSegueIdentifier]) {
        PlaylistViewController *controller = segue.destinationViewController;
        if (controller) {
            controller.selectedPlaylistIndex = self.selectedPlaylistIndex;
            controller.playlists = self.playlists;
        }
    }
}


#pragma mark - MusicPlayerDelegate
- (void)didPassPlaylists:(NSMutableArray <Playlist *>*)playlists playlistIndex:(NSInteger)index {
    self.playlists = playlists;
    self.selectedPlaylistIndex = index;
    [self performSegueWithIdentifier:self.PlaylistSegueIdentifier sender:self];
}

@end
