//
//  PlaylistMenuViewController.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import "Playlist.h"
#import "PlaylistMenuTableViewModel.h"
#import "MusicPlayerDelegate.h"
#import "PlaylistViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaylistMenuViewController : UIViewController <MusicPlayerDelegate>

/** Outlets */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** Properties */
@property (nonatomic) PlaylistMenuTableViewModel *viewModel;
@property (nonatomic) NSMutableArray *playlists;
@property (nonatomic) NSInteger selectedPlaylistIndex;
@property (nonatomic) NSString *PlaylistSegueIdentifier;

@end

NS_ASSUME_NONNULL_END
