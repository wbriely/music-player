//
//  PlaylistMenuTableViewModel.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseTableViewModel.h"
#import "Playlist.h"
#import "MusicPlayerDelegate.h"
#import "PlaylistManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaylistMenuTableViewModel : BaseTableViewModel <UITextFieldDelegate>

/** Properties */
@property (nonatomic) NSString *cellIdentifier;
@property (nonatomic) UITextField *playlistTextField;
@property (nonatomic) NSInteger selectedPlaylistIndexPath;
@property (nonatomic, weak) id<MusicPlayerDelegate> delegate;
@property (nonatomic) Playlist *playlist;

@end

NS_ASSUME_NONNULL_END
