//
//  PlaylistMenuTableViewModel.m
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "PlaylistMenuTableViewModel.h"

@implementation PlaylistMenuTableViewModel

- (NSString *)cellIdentifier {
    return @"PlaylistMenuCell";
}

- (void)setTableView:(UITableView *)tableView {
    [super setTableView:tableView];
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[PlaylistManager sharedPlaylist] playlists] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:self.cellIdentifier];
    }
    Playlist *playlist = [[PlaylistManager sharedPlaylist] playlists][indexPath.row];
    cell.textLabel.text = playlist.name;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [[[PlaylistManager sharedPlaylist] playlists] removeObjectAtIndex:indexPath.row];
    [self.tableView reloadData];
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    self.selectedPlaylistIndexPath = indexPath.row;
    Playlist *playlist = [[PlaylistManager sharedPlaylist] playlists][self.selectedPlaylistIndexPath];
    self.playlist = playlist;
    [self.delegate didPassPlaylists:[[PlaylistManager sharedPlaylist] playlists] playlistIndex:self.selectedPlaylistIndexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 56;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 56)];
    self.playlistTextField = [[UITextField alloc] init];
    self.playlistTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.playlistTextField.textColor = [UIColor whiteColor];
    self.playlistTextField.font = [UIFont systemFontOfSize:20];
    self.playlistTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Playlist Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor], NSFontAttributeName : [UIFont italicSystemFontOfSize:20]}];
    self.playlistTextField.returnKeyType = UIReturnKeyDone;
    self.playlistTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.playlistTextField.clearButtonMode = true;
    self.playlistTextField.delegate = self;
    [view addSubview:self.playlistTextField];
    [view setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:147.0/255.0 alpha:1.0]];

    [view addConstraints:
     @[
       [NSLayoutConstraint constraintWithItem:self.playlistTextField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0],
       [NSLayoutConstraint constraintWithItem:self.playlistTextField attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0],
       [NSLayoutConstraint constraintWithItem:self.playlistTextField attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:20],
       [NSLayoutConstraint constraintWithItem:self.playlistTextField attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-20]
       ]];
    return view;
}


#pragma mark - UIScrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.playlistTextField resignFirstResponder];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSError *error;
    Playlist *playlistTwo = [[Playlist alloc] initWithIdentifier:2 name:textField.text songs:[[NSMutableArray alloc] init] error:&error];
    [[[PlaylistManager sharedPlaylist] playlists] addObject:playlistTwo];
    [self.tableView reloadData];
    self.playlistTextField.text = @"";
    [self.playlistTextField resignFirstResponder];
    return YES;
}

@end
