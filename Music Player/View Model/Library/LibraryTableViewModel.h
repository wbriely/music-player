//
//  LibraryTableViewModel.h
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BaseTableViewModel.h"
#import "SongManager.h"
#import "MusicPlayerDelegate.h"
#import "PlaylistTableViewModel.h"
#import "PlaylistManager.h"
#import "Formatting.h"

NS_ASSUME_NONNULL_BEGIN

@interface LibraryTableViewModel : BaseTableViewModel <UISearchBarDelegate, MusicPlayerDelegate>

/** Properties */
@property (nonatomic) NSString *cellIdentifier;
@property (nonatomic) NSInteger selectedSongIndexPath;
@property (nonatomic, weak) id<MusicPlayerDelegate> delegate;
@property (nonatomic) UISearchBar *searchBar;
@property (nonatomic) BOOL sortByTitle;
@property (nonatomic) BOOL sortByArtist;
@property (nonatomic) NSArray <Song *> *filteredSongs;
@property (nonatomic) PlaylistTableViewModel *playlistViewModel;

@end

NS_ASSUME_NONNULL_END
