//
//  LibraryTableViewModel.m
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "LibraryTableViewModel.h"

@implementation LibraryTableViewModel

- (NSString *)cellIdentifier {
    return @"LibraryCell";
}

- (void)setTableView:(UITableView *)tableView {
    [super setTableView:tableView];
    self.filteredSongs = [[SongManager sharedManager] songs];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredSongs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:self.cellIdentifier];
    }
    NSSortDescriptor *titleSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    self.filteredSongs = [self.filteredSongs sortedArrayUsingDescriptors:@[titleSortDescriptor]];
    Song *song = self.filteredSongs[indexPath.row];
    cell.textLabel.text = song.title;
    cell.textLabel.font = [UIFont systemFontOfSize:30];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Artist: %@", song.artist];
    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    self.selectedSongIndexPath = indexPath.row;
    NSArray <Song *> *songList = self.filteredSongs;
    Song *song = songList[self.selectedSongIndexPath];
    [self.delegate didPassSongList:songList songIndex:self.selectedSongIndexPath];
    Playlist *playlist = [[PlaylistManager sharedPlaylist] playlists][self.playlistViewModel.selectedPlaylistIndex];
    NSMutableArray <Song *> *PlaylistSongList = playlist.songs;
    for (Song *song in songList) {
        for (Song *playlistSong in PlaylistSongList) {
            if (playlistSong.player.isPlaying) {
                [playlistSong.player stop];
            } else if (song.player.isPlaying) {
                [song.player stop];
            }
        }
    }
    [song.player play];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSSortDescriptor *titleSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSSortDescriptor *artistSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"artist" ascending:YES];
    if (self.sortByArtist && !self.sortByTitle) {
        self.filteredSongs = [self.filteredSongs sortedArrayUsingDescriptors:@[artistSortDescriptor]];
        Song *song = self.filteredSongs[indexPath.row];
        cell.textLabel.text = song.title;
        cell.textLabel.font = [UIFont systemFontOfSize:30];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Artist: %@", song.artist];
    } else if (!self.sortByArtist && self.sortByTitle) {
        self.filteredSongs = [self.filteredSongs sortedArrayUsingDescriptors:@[titleSortDescriptor]];
        Song *song = self.filteredSongs[indexPath.row];
        cell.textLabel.text = song.title;
        cell.textLabel.font = [UIFont systemFontOfSize:30];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Artist: %@", song.artist];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];
    UIColor *blueBackground = [UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:147.0/255.0 alpha:1.0];
    [view setBackgroundColor:blueBackground];
    self.searchBar = [[UISearchBar alloc] init];
    self.searchBar.translatesAutoresizingMaskIntoConstraints = false;
    [self.searchBar setBackgroundImage:[UIImage new]];
    [self.searchBar setBarTintColor:blueBackground];
    self.searchBar.placeholder = @"Search";
    self.searchBar.delegate = self;

    UIButton *sortByTitleButton = [[UIButton alloc] init];
    [sortByTitleButton setBackgroundColor:[UIColor grayColor]];
    [sortByTitleButton setTitle:@"Sort by Title" forState:UIControlStateNormal];
    [sortByTitleButton setTitleColor:blueBackground forState:UIControlStateNormal];
    sortByTitleButton.layer.cornerRadius = 5;
    if (!self.sortByArtist && self.sortByTitle) {
        [sortByTitleButton setBackgroundColor:[UIColor grayColor]];
    } else if (!self.sortByTitle && self.sortByArtist) {
        [sortByTitleButton setBackgroundColor:[UIColor whiteColor]];
    }
    sortByTitleButton.clipsToBounds = true;
    [sortByTitleButton addTarget:self action:@selector(didPressSortByTitleButton) forControlEvents:UIControlEventTouchUpInside];
    sortByTitleButton.translatesAutoresizingMaskIntoConstraints = false;

    UIButton *sortByArtistButton = [[UIButton alloc] init];
    [sortByArtistButton setBackgroundColor:[UIColor whiteColor]];
    if (self.sortByArtist && !self.sortByTitle) {
        [sortByArtistButton setBackgroundColor:[UIColor grayColor]];
    } else if (self.sortByTitle && !self.sortByArtist) {
        [sortByArtistButton setBackgroundColor:[UIColor whiteColor]];
    }
    [sortByArtistButton addTarget:self action:@selector(didPressSortByArtistButton) forControlEvents:UIControlEventTouchUpInside];
    sortByArtistButton.layer.cornerRadius = 5;
    [sortByArtistButton setTitleColor:blueBackground forState:UIControlStateNormal];
    [sortByArtistButton setTitle:@"Sort by Artist" forState:UIControlStateNormal];
    sortByArtistButton.clipsToBounds = true;
    sortByArtistButton.translatesAutoresizingMaskIntoConstraints = false;
    [view addSubview:sortByArtistButton];
    [view addSubview:sortByTitleButton];
    [view addSubview:self.searchBar];

    [view addConstraints:
  @[
    [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0],
    [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0],
    [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0],
    [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:56],
    [NSLayoutConstraint constraintWithItem:sortByTitleButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.searchBar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:4],
    [NSLayoutConstraint constraintWithItem:sortByTitleButton attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:30],
    [NSLayoutConstraint constraintWithItem:sortByTitleButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-12],
    [NSLayoutConstraint constraintWithItem:sortByTitleButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-12],
    [NSLayoutConstraint constraintWithItem:sortByArtistButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.searchBar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:4],
    [NSLayoutConstraint constraintWithItem:sortByArtistButton attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:12],
    [NSLayoutConstraint constraintWithItem:sortByArtistButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-30],
    [NSLayoutConstraint constraintWithItem:sortByArtistButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-12]
    ]];

    return view;
}


#pragma mark - UIScrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.searchBar resignFirstResponder];
}


#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSMutableArray <Song *> *tempSongs = [NSMutableArray new];
    for (Song *song in [[SongManager sharedManager] songs]) {
        if ([song.title.lowercaseString containsString:searchText.lowercaseString] || [song.artist.lowercaseString containsString:searchText.lowercaseString]) {
            [tempSongs addObject:song];
        }
    }
    self.filteredSongs = tempSongs;

    if ([searchText  isEqual: @""]) {
        self.filteredSongs = [[SongManager sharedManager] songs];
    }
    [self.tableView reloadData];
}


#pragma mark - Actions
- (void)didPressSortByTitleButton {
    self.sortByTitle = true;
    self.sortByArtist = false;
    [self.tableView reloadData];
}

- (void)didPressSortByArtistButton {
    self.sortByTitle = false;
    self.sortByArtist = true;
    [self.tableView reloadData];
}

@end

