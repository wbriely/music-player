//
//  AddToPlaylistTableViewModel.h
//  Music Player
//
//  Created by Will Briely on 27/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "BaseTableViewModel.h"
#import "PlaylistManager.h"
#import "Playlist.h"
#import "MusicPlayerDelegate.h"
#import "LibraryTableViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddToPlaylistTableViewModel : BaseTableViewModel

/** Properties */
@property (nonatomic) NSString *cellIdentifier;
@property (nonatomic, weak) id<MusicPlayerDelegate> delegate;
@property (nonatomic) Song *songToPlaylist;

/**
 Adding song to playlist

 @param song Song
 */
- (void)songToAddToPlaylist:(Song *)song;

@end

NS_ASSUME_NONNULL_END
