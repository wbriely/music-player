//
//  AddToPlaylistTableViewModel.m
//  Music Player
//
//  Created by Will Briely on 27/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "AddToPlaylistTableViewModel.h"

@implementation AddToPlaylistTableViewModel

- (void)setTableView:(UITableView *)tableView {
    [super setTableView:tableView];
}

- (NSString *)cellIdentifier {
    return @"AddToPlaylistCell";
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[PlaylistManager sharedPlaylist] playlists] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:self.cellIdentifier];
    }
    Playlist *playlist = [[PlaylistManager sharedPlaylist] playlists][indexPath.row];
    cell.textLabel.text = playlist.name;
    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    Playlist *playlist = [[PlaylistManager sharedPlaylist] playlists][indexPath.row];
    [playlist.songs addObject:self.songToPlaylist];
    [self.delegate didPressPlaylist:playlist];
}


#pragma mark - Adding Song to Playlist
- (void)songToAddToPlaylist:(Song *)song {
    self.songToPlaylist = song;
}

@end
