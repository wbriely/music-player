//
//  BaseTableViewModel.h
//  Music Player
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewModel : NSObject <UITableViewDataSource, UITableViewDelegate>

/** Properties */
@property (weak, nonatomic) UITableView *tableView;

/**
 Initialising table view

 @param tableView UITableView
 @return instancetype
 */
- (instancetype)initWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
