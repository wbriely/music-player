//
//  PlaylistTableViewModel.m
//  Music Player
//
//  Created by Will Briely on 27/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "PlaylistTableViewModel.h"

@implementation PlaylistTableViewModel

- (NSString *)cellIdentifier {
    return @"PlaylistCell";
}

- (void)setTableView:(UITableView *)tableView {
    [super setTableView:tableView];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.selectedPlaylist.songs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:self.cellIdentifier];
    }
    Song *song = self.selectedPlaylist.songs[indexPath.row];
    cell.textLabel.text = song.title;
    cell.textLabel.font = [UIFont systemFontOfSize:30];

    NSString *formattedDuration = [Formatting stringFromTimeInterval:song.player.duration];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Duration: %@", formattedDuration];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.selectedPlaylist.songs removeObjectAtIndex:indexPath.row];
    [self.tableView reloadData];
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    self.selectedSongIndex = indexPath.row;
    NSMutableArray <Song *> *songList = self.selectedPlaylist.songs;
    Song *song = self.selectedPlaylist.songs[indexPath.row];
    [self.delegate didPassSongList:songList songIndex:self.selectedSongIndex];
    for (Song *playlistSong in songList) {
        for (Song *song in [[SongManager sharedManager] songs]) {
            if (playlistSong.player.isPlaying) {
                [playlistSong.player stop];
            } else if (song.player.isPlaying) {
                [song.player stop];
            }
        }
    }
    [song.player play];
}


#pragma mark - Playlist Passing
- (void)selectedPlaylist:(Playlist *)playlist; {
    self.selectedPlaylist = playlist;
}

@end
