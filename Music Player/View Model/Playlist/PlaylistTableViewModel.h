//
//  PlaylistTableViewModel.h
//  Music Player
//
//  Created by Will Briely on 27/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import "BaseTableViewModel.h"
#import "PlaylistManager.h"
#import "Playlist.h"
#import "PlaylistMenuTableViewModel.h"
#import "MusicPlayerDelegate.h"
#import "SongManager.h"
#import "Formatting.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaylistTableViewModel : BaseTableViewModel

/** Properties */
@property (nonatomic) NSString *cellIdentifier;
@property (nonatomic) NSInteger selectedPlaylistIndex;
@property (nonatomic) NSInteger selectedSongIndex;
@property (nonatomic) PlaylistMenuTableViewModel *playlistMenuViewModel;
@property (nonatomic, weak) id<MusicPlayerDelegate> delegate;
@property (nonatomic) Playlist *selectedPlaylist;

/**
 Passes selected playlist

 @param playlist Playlisr
 */
- (void)selectedPlaylist:(Playlist *)playlist;

@end

NS_ASSUME_NONNULL_END
