//
//  MusicPlayerDelegate.h
//  Music Player
//
//  Created by Will Briely on 26/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Song.h"
#import "Playlist.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MusicPlayerDelegate <NSObject>

@optional

/**
 Pass list of songs

 @param songList NSArray <Song *>
 @param index NSInteger
 */
- (void)didPassSongList:(NSArray <Song *>*)songList songIndex:(NSInteger)index;

/**
 Pass list of playlists

 @param playlists NSMutableArray <Playlist>
 @param index NSInteger
 */
- (void)didPassPlaylists:(NSMutableArray <Playlist *>*)playlists playlistIndex:(NSInteger)index;

/**
 Passing the selected playlist

 @param playlist Playlist
 */
- (void)didPressPlaylist:(Playlist *)playlist;

@end

NS_ASSUME_NONNULL_END
