//
//  Music_PlayerUITests.m
//  Music PlayerUITests
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Music_PlayerUITests : XCTestCase

@property (nonatomic) XCUIApplication *app;
@property (nonatomic) XCUIElementQuery *tableView;

@end

@implementation Music_PlayerUITests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;

    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    self.app = [[XCUIApplication alloc] init];
    self.tableView = self.app.tables;

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testPlayback {

    /// Testing playback buttons

    /// Taps the 'Afraid of Destiny' song
    [self.tableView.cells.staticTexts[@"Afraid of Destiny"] tap];

    /// Taps the 'Pause' button
    [self.app.buttons[@"Pause"] tap];

    /// Taps the 'Play' button
    [self.app.buttons[@"Play"] tap];

    /// Taps the 'Fast Forward' button
    [self.app.buttons[@"Fast Forward"] tap];

    /// Taps the 'Rewind' button
    [self.app.buttons[@"Rewind"] tap];

    /// Taps the 'Shuffle' button
    [self.app.buttons[@"Shuffle"] tap];

    /// Taps the 'Add To Playlist' button
    [self.app.buttons[@"Add To Playlist"] tap];

    /// Taps the 'Cancel' button
    [self.app.buttons[@"Cancel"] tap];

    /// Taps the 'Library' (back) button
    [self.app.navigationBars[@"Playback"].buttons[@"Library"] tap];

}

- (void)testSortByButtons {

    /// Taps the 'Sort By Title' button
    [self.tableView.buttons[@"Sort By Title"] tap];

    /// Taps the 'Sort By Artist' button
    [self.tableView.buttons[@"Sort By Artist"] tap];

}

- (void)testSearchBar {

    /// Accessing the UISearchBar
    XCUIElement *searchBar = self.tableView.searchFields[@"Search"];

    /// Taps the UISearchBar
    [searchBar tap];

    /// Taps the 'B' key
    [self.app.keyboards.keys[@"B"] tap];

    /// Taps the 'o' key
    [self.app.keyboards.keys[@"o"] tap];

    /// Taps the 'delete' key
    [self.app.keyboards.keys[@"delete"] tap];

    /// Taps the 'Search' button
    [self.app.keyboards.buttons[@"Search"] tap];

    /// Taps the 'Clear text' button
    [searchBar.buttons[@"Clear text"] tap];

}

- (void)testLicenceButton {

    /// Taps the 'Licence' button
    [[[self.app.navigationBars[@"Library"] childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:1] tap];

    /// Taps the 'Library' (back) button
    [self.app.navigationBars[@"Licence"].buttons[@"Library"] tap];

}

- (void)testAddNewPlayist {

    /// Taps 'Playlist' button within navigation bar
    [[[self.app.navigationBars[@"Library"] childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:0] tap];

    /// Taps 'Enter Playlist Name' textfield
    [self.app.tables.textFields[@"Enter Playlist Name"] tap];

    /// Taps 'P' key
    [self.app.keyboards.keys[@"P"] tap];

    /// Taps 'l' key
    XCUIElement *lKey = self.app.keyboards.keys[@"l"];
    [lKey tap];

    /// Taps 'a' key
    [self.app.keyboards.keys[@"a"] tap];

    /// Taps 'y' key
    [self.app.keyboards.keys[@"y"] tap];

    /// Taps 'l' key
    [lKey tap];

    /// Taps 'i' key
    [self.app.keyboards.keys[@"i"] tap];

    /// Taps 's' key
    [self.app.keyboards.keys[@"s"] tap];

    /// Taps 't' key
    [self.app.keyboards.keys[@"t"] tap];

    /// Taps 'space' key
    [self.app.keyboards.keys[@"space"] tap];

    /// Taps 'more' key
    [self.app.keyboards.keys[@"more"] tap];

    /// Taps '2' key
    [self.app.keyboards.keys[@"2"] tap];

    /// Taps 'Done' button
    [self.app.keyboards.buttons[@"Done"] tap];

    /// Taps 'Playlist 2' cell (newly created playlist)
    [self.app.tables.cells.staticTexts[@"Playlist 2"] tap];

}

- (void)testAddSongToPlaylist {

    /// Creates new playlist
    [self testAddNewPlayist];

    /// Goes back to the playlist menu screen
    [self.app.navigationBars[@"Playlist"].buttons[@"Playlists"] tap];

    /// Goes back to the library screen
    [self.app.navigationBars[@"Playlists"].buttons[@"Library"] tap];

    /// Taps the 'Afraid of Destiny' song
    [self.tableView.cells.staticTexts[@"Afraid of Destiny"] tap];

    /// Taps 'Add To Playlist' button
    [self.app.buttons[@"Add To Playlist"] tap];

    /// Taps 'Playlist 2' playlist
    XCUIElement *playlist2StaticText = self.tableView.cells.staticTexts[@"Playlist 2"];
    [playlist2StaticText tap];

    /// Goes back to the library screen
    XCUIElement *playbackNavigationBar = self.app.navigationBars[@"Playback"];
    [playbackNavigationBar.buttons[@"Library"] tap];

    /// Taps 'Playlist' button within navigation bar
    [[[self.app.navigationBars[@"Library"] childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:0] tap];

    /// Taps 'Playlist 2' playlist
    [playlist2StaticText tap];

    /// Taps the 'Afraid of Destiny' song (newly added song)
    [self.tableView.cells.staticTexts[@"Afraid of Destiny"] tap];


}

@end
