//
//  Music_PlayerTests.m
//  Music PlayerTests
//
//  Created by Will Briely on 25/02/2019.
//  Copyright © 2019 petsathome. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Song.h"
#import "SongManager.h"
#import "Playlist.h"
#import "PlaylistManager.h"

@interface Music_PlayerTests : XCTestCase

@end

@implementation Music_PlayerTests

- (void)testSongManager {

    SongManager *manager = [[SongManager alloc] init];

    /// Testing songs exist
    XCTAssertGreaterThan(manager.songs.count, 0);

    /// Testing no data required for the song is missing
    for (Song *song in manager.songs) {
        XCTAssertNotNil(song.title);
        XCTAssertNotNil(song.fileName);
        XCTAssertNotNil(song.fileType);
        XCTAssertNotNil(song.artist);
        XCTAssertGreaterThanOrEqual(song.identifier, 0);
    }
}

- (void)testPlaylistManager {

    PlaylistManager *manager = [[PlaylistManager alloc] init];

    /// Testing playlists exist
    XCTAssertGreaterThan(manager.playlists.count, 0);

    /// Testing no data required to make a playlist is missing
    for (Playlist *playlist in manager.playlists) {
        XCTAssertNotNil(playlist.name);
        XCTAssertGreaterThanOrEqual(playlist.identifier, 0);
        XCTAssertNotNil(playlist.songs);
    }
}

@end
